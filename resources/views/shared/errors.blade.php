@if (count($errors))
    <div id="form-errors">
        <p>The following errors have occured:</p>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif