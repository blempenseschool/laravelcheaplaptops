@extends('layouts.main')

@section('promo')
    <section id="promo">
        <div id="promo-details">
            <h1>Todays Deals</h1>
            <p>Checkout this section of <br> products at a discounted price.</p>
            <a href="#" class="default-btn">Shop now</a>
        </div>
        <img src="{{ asset('img/promo.png') }}" alt="Promotional Ad">
    </section>
@endsection

@section('content')
    <h2>New products</h2>
    <hr>
    <div id="products">
        @foreach($products as $product)
            @include('store.product-teaser')
        @endforeach
    </div>
@endsection