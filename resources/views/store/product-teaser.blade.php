<div class="product">
    <a href="{{ route('store.show', $product->id) }}">
        <img src="{{ asset($product->image) }}" alt="{{ $product->title }}" width="240" class="feature">
    </a>
    <h3>
        <a href="{{ route('store.show', $product->id) }}">{{ $product->title }}</a>
    </h3>
    <p>
        {{ str_limit($product->description, 200) }}
    </p>
    <h5>Availability: {!! $product->present()->showAvailability !!}</h5>
    @if($product->availability)
        <p>
            <a href="#" class="cart-btn">
                <span class="price">${{ $product->price }}</span>
                <img src="{{ asset('img/white-cart.gif') }}" alt="Add To Cart">
                ADD TO CART
            </a>
        </p>
    @endif
</div>