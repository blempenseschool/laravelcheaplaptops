<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'StoreController@index')->name('store.index');
Route::get('product/{id}', 'StoreController@show')->name('store.show');
Route::get('category/{id}', 'StoreController@category')->name('store.category');

Route::group(['prefix'=>'admin', 'as'=>'admin.'], function() {
   Route::resource('categories', 'Admin\CategoriesController',
       ['only'=>['index', 'store', 'destroy']]);

    Route::resource('products', 'Admin\ProductsController',
       ['only'=>['index', 'store', 'destroy']]);
    Route::post('products/toggle-availability', 'Admin\ProductsController@toggleAvailability')
        ->name('products.toggle');
});
