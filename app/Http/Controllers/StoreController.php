<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index() {
        return view('store.index')
            ->with('products', Product::take(4)->orderBy('created_at', 'desc')->get());
    }

    public function show($id) {
        return view('store.show')->with('product', Product::findOrFail($id));
    }

    public function category($id) {
        $cat = Category::findOrFail($id);
        return view('store.category')
            ->with('products', $cat->products()->paginate(6))
            ->with('category', $cat);
    }
}
